# Instalación NS2 (Network Simulator) y parche OLSR y ZRP 

Simulador de redes opensource para investigacion..

Compatibilidad con sistemas:

# UNIX
  - OS X
  - Linux
  - Solaris
# Windows
  - Cygwin

### Instalar dependencias y actualizaciones

Ahora necesita descargar algunos paquetes esenciales para ns2, estos paquetes se pueden descargar mediante los siguientes comandos:

```sh
$ sudo apt-get update
$ sudo apt-get gcc
$ sudo apt-get install build-essential autoconf automake
$ sudo apt-get install tcl8.5-dev tk8.5-dev
$ sudo apt-get install perl xgraph libxt-dev libx11-dev libxmu-dev
```

### Installatión

Descargar ns-allinone-2.35_gcc482.tar y mover en ~/ extraer o ejercutar el siguiente comando:

```sh
$ tar xfz ns-allinone-2.35_gcc482.tar
```

Descargar los parches para OLSR (umolsr_for-zrp-patched_ns235), ZRP (zrp-ns235.patch) y copiar en ~/ns-allinone-2.35/, ejecutar los siguientes comandos para aplicar los parches: 

```sh
$ patch -p0 < zrp-ns235.patch
$ patch -p0 < umolsr_for-zrp-patched_ns235.patch
$ ./install
```

Después de completar la instalación, Abra el archivo bashrc para establecer las variables de entorno, escriba el siguiente comando:

```sh
$ sudo gedit ~/.bashrc
```

Ahora aparece una ventana del editor, copie y pegue los siguientes códigos al final del archivo de texto (tenga en cuenta que '/home/egaviria/ns-allinone-2.35/octl-1.14' en cada línea en el siguiente código debe estar reemplazado con su ubicación donde se extrae el archivo 'ns-allinone-2.35.tar.gz'), puede usar el comando whoami para ver el nombre de usuario

```sh
# LD_LIBRARY_PATH
OTCL_LIB=/home/egaviria/ns-allinone-2.35/otcl-1.14
NS2_LIB=/home/egaviria/ns-allinone-2.35/lib
X11_LIB=/usr/X11R6/lib
USR_LOCAL_LIB=/usr/local/lib
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$OTCL_LIB:$NS2_LIB:$X11_LIB:$USR_LOCAL_LIB
```

```sh
# TCL_LIBRARY
TCL_LIB=/home/egaviria/ns-allinone-2.35/tcl8.5.10/library
USR_LIB=/usr/lib
export TCL_LIBRARY=$TCL_LIB:$USR_LIB
```

```sh
# PATH
XGRAPH=/home/egaviria/ns-allinone-2.35/bin:/home/egaviria/ns-allinone-2.35/tcl8.5.10/unix:/home/egaviria/ns-allinone-2.35/tk8.5.10/unix
NS=/home/egaviria/ns-allinone-2.35/ns-2.35/
NAM=/home/egaviria/ns-allinone-2.35/nam-1.15/
PATH=$PATH:$XGRAPH:$NS:$NAM
```

Guarda y cierra el editor de texto

Cierre la ventana de terminal e inicie una nueva ventana de terminal y ahora cambie el directorio a ns-2.35 y valide ns-2.35 mediante el siguiente comando (demora de 30 a 45 minutos)

```sh
$ cd ns-2.35
$ ./validate
```

Si la instalación se realiza correctamente, podrá ver% en el símbolo del sistema mientras escribe el siguiente comando

```sh
$ ns
```


#### Documentacion oficial NS2 

Documentacion oficial ns [www.isi.edu](https://www.isi.edu/nsnam/ns/)

### Colaboradores

 - Esteban Gaviria Restrepo
 - Angela Lizeth Muñoz Obonaga

Licencia
----

MIT


**Software libre, Disfruta!**